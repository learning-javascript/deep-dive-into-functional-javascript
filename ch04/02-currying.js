const f = (a, b) => a * b
console.log('1)', f(2, 4));

// currying f
const fCurried = a => b => a * b;
const g = fCurried(2); // g is a new function with arity of 1
console.log('2)', g(4));
console.log('3)', fCurried(2)(4));

const prod = (a, b, c) => a * b * c;
const prodCurried = a => b => c => a * b * c;
console.log('4)', prod(2, 3, 4));
console.log('5)', prodCurried(2)(3)(4));

const mul6 = prodCurried(2)(3);
console.log('6)', mul6(4));

const _ = require('lodash');
const prodCurriedLo = _.curry(prod);
console.log('7)', prodCurriedLo(2)(3)(4));
// lodash gives more flexibility and provides different arity curried functions
console.log('8)', prodCurriedLo(2, 3)(4));
// using _ you can apply arguments in any order
console.log('9)', prodCurriedLo(_, _, 4)(_, 3)(2));

function mapScoresToGrades(tests, average) {
    let getGradeTest = average => testScrore => testScrore >= average ? 'A' : 'F';
    let gradeTest = getGradeTest(average);
    return tests.map(gradeTest);
}
let grades = mapScoresToGrades([0.3, 0.8, 1], 0.7);
console.log('10)', grades);
grades = mapScoresToGrades([0.3, 0.8, 1], 0.9);
console.log('11)', grades);

const getGradeTest = average => testScrore => testScrore >= average ? 'A' : 'F';
const tester = getGradeTest(0.8);
console.log('12)', tester(0.79));
console.log('13)', tester(0.81));

const newTester = getGradeTest(0.5);
console.log('14)', newTester(0.79));

const getGradeTest2 =
    (passGrade, failGrade) =>
    average =>
    testScrore => testScrore >= average ? passGrade : failGrade;

const passFaileTester = getGradeTest2('Pass', 'Fail')(0.2);
console.log('15)', passFaileTester(0.19), passFaileTester(0.21));

const uncurriedGetGradeTest = (passGrade, failGrade, average, testScrore) => testScrore >= average ? passGrade : failGrade;
console.log('16)', uncurriedGetGradeTest('+', '-', 0.5, 0.51));

// curried function vs uncurried
// WET - we enjoy typing
console.log('===== WET uncurried =====');
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.46));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.47));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.48));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.49));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.5));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.51));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.52));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.53));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.54));
console.log(uncurriedGetGradeTest('+', '-', 0.5, 0.55));

// DRY - don't repeat yourself
console.log('===== DRY curried =====');
const gradeTest = passGrade => failGrade => average => testScrore => testScrore >= average ? passGrade : failGrade;
const tester2 = gradeTest('+')('-')(0.5);
console.log(tester2(0.46));
console.log(tester2(0.47));
console.log(tester2(0.48));
console.log(tester2(0.49));
console.log(tester2(0.5));
console.log(tester2(0.51));
console.log(tester2(0.52));
console.log(tester2(0.53));
console.log(tester2(0.54));
console.log(tester2(0.55));
