const _ = require('lodash');

const volume = (a, b, c) => a * b * c;
console.log('1)', volume(2, 3, 4));

const curriedVolume = _.curry(volume);
console.log('2)', curriedVolume(2)(3)(4));

let partialVolume_2_3 = _.partial(volume, 2, 3);
console.log('3)', partialVolume_2_3(4));

// it is same as this
partialVolume_2_3 = curriedVolume(2)(3);
console.log('4)', partialVolume_2_3(4));

// implementation of partial
const partial = (f, ...argList1) => (...argList2) => f(...argList1, ...argList2);

console.log('5)', partial(volume, 2, 3)(4));
console.log('6)', partial(volume, 2)(3, 4));
console.log('7)', partial(partial(volume, 2), 3)(4));

const curry1 = f => a => (...rest) => f(a, ...rest);
const curry2 = f => a => b => (...rest) => f(a, b, ...rest);

console.log('8)', curry1(volume)(2)(3, 4));
console.log('9)', curry2(volume)(2)(3)(4));

const curry = f => curryN(f, []);
const curryN = (f, acc) => acc.length === f.length ? f(...acc) : arg => curryN(f, [...acc, arg]);
console.log('10) partial', partial(volume, 2, 3)(4));
console.log('11) curry', curry(volume)(2)(3)(4));

// binding
console.log('12)', volume.bind(null, 2, 3)(4));
console.log('13)', volume.bind(null, 2)(3, 4));

// implementing partial and curry with bind
const partialWithBind = (f, ...args) => f.bind(null, ...args);
const curryWithBind = f => a => f.length === 1 ? f(a) : curryWithBind(partialWithBind(f, a));

// implementing map
const map = f => ([head, ...tail]) => typeof head === 'undefined' ? [] : [f(head), ...map(f)(tail)];
console.log('14)', map(a => 2 * a)([1, 2, 3, 4]));
