// function arity
console.log('arity', (a => a * 2).length);

sum = (a, b) => a + b;
console.log('arity', sum.length);
console.log('arity', sum.bind(null, 5).length);

console.log('arity', ((a, b = 0) => a + b).length);

// cannot rely on function length in case of rest parameters it is 0
// this is called variadic function
sumArgs = (...args) => args.reduce(sum, 0);
console.log(sumArgs.length);

// this is how it is possible to implement variadic function in ES5
sumArgs2 = function() {
    // next line cause an error, because arguments is not an array and doesn't have array functions
    // return arguments.reduce(sum, 0);
    const args = Array.prototype.slice.call(arguments);
    return args.reduce(sum, 0);
};
console.log(sumArgs2(2, 3, 4));
