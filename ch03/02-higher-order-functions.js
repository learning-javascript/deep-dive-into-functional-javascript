// functions to handle arrays
const append = ([head1, ...tail1], array2) => typeof head1 === 'undefined' ? array2 : [head1, ...append(tail1, array2)];
console.log(append([1, 2, 3], [4, 5]));

const arr1 = [1, 2, 3];
const arr2 = [4, 5];

console.log(arr1.concat(arr2));

console.log(arr1.concat(arr2).every(x => x % 2 === 1));
console.log(arr1.concat(arr2).some(x => x % 2 === 1));
console.log(arr1.concat(arr2).find(x => x % 2 === 1));
console.log(arr1.concat(arr2).join(','));
console.log(arr1.concat(arr2).join(',').split(','));
console.log(arr1.concat(arr2).join(',').split(',').reverse());

const formatCurrency = function(currencySymbol, decimalSeparator) {
    return function (value) {
        const wholePart = Math.trunc(value / 100);
        let fractionPart = value % 100;
        if (fractionPart < 10) fractionPart = '0' + fractionPart;
        return `${currencySymbol}${wholePart}${decimalSeparator}${fractionPart}`;
    }
};
const formatter = formatCurrency('€', ',');
const currencies = [0, 1209, 10000, 10010];

// traditional approach
for (let i = 0; i < currencies.length; i++) {
    console.log(formatter(currencies[i]));
}

// using for each loop
for (let value of currencies) {
    console.log(formatter(value));
}

// using forEach function
currencies.forEach(value => console.log(formatter(value)));
currencies.forEach(console.log);

// EXAMPLES OF BAD PRACTICE
// Return an array of formatted values
let formattedCurrencies = [];
currencies.forEach(value => {
    formattedCurrencies.push(formatter(value));
});
console.log(formattedCurrencies);

// Calculate the sum of the currencies array
let sum = 0;
currencies.forEach(value => {
    sum += value;
});
console.log('sum', sum);

// Filter expensive items (>= 10000)
let expensiveItems = [];
currencies.forEach(value => {
    if (value >= 10000) expensiveItems.push(value);
});
console.log('expensiveItems', expensiveItems);
