// map function
const square = x => x ** 2;

console.log([1, 2, 3, 4].map(square));

const ownMap = ([head, ...tail], f) => 
    typeof head === 'undefined' ? [] : [f(head), ...ownMap(tail, f)];

console.log(ownMap([1, 2, 3, 4], square));

// reduce function
const sum = (x, y) => x + y;
console.log([1, 2, 3, 4].map(square).reduce(sum, 0));

const ownReduce = ([head, ...tail], f, accumulator) =>
    typeof head === 'undefined' ? accumulator : ownReduce(tail, f, f(accumulator, head));

console.log(ownReduce([1, 2, 3, 4], sum, 0));

// filter function
const ownFilter = ([head, ...tail], predicate) =>
    typeof head === 'undefined' ? [] : predicate(head) ?
        [head, ...ownFilter(tail, predicate)] : ownFilter(tail, predicate);

console.log(ownFilter([1, 2, 3, 4, 5], x => x % 2 === 1));

const twoOrHigher = x => x >= 2;
console.log([1, 2, 3, 4].map(square).filter(twoOrHigher).reduce(sum, 0));

// fixing forEach solution from previous example
const formatCurrency = function(currencySymbol, decimalSeparator) {
    return function (value) {
        const wholePart = Math.trunc(value / 100);
        let fractionPart = value % 100;
        if (fractionPart < 10) fractionPart = '0' + fractionPart;
        return `${currencySymbol}${wholePart}${decimalSeparator}${fractionPart}`;
    }
};
const formatter = formatCurrency('€', ',');
const currencies = [0, 1209, 10000, 10010];

const formattedCurrencies = currencies.map(formatter);
console.log('formattedCurrencies', formattedCurrencies);

const currenciesSum = currencies.reduce(sum, 0);
console.log('currenciesSum', currenciesSum);

const expensiveCurrencies = currencies.filter(v => v >= 10000);
console.log('expensiveCurrencies', expensiveCurrencies);
