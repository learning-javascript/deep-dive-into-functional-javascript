const ownFilter = (array, filterFunction) => {
    const reducer = (accumulator, element) => filterFunction(element) ?
        [...accumulator, element] : accumulator;
    return array.reduce(reducer, []);
}

console.log(ownFilter([1, 2, 3, 4, 5], x => x < 4));

require('console.table');
const students = [
    {
        id: 1,
        name: 'Marcus',
        birtDate: '1997-12-11'
    },
    {
        id: 2,
        name: 'Andrea',
        birtDate: '1993-09-08'
    },
    {
        id: 3,
        name: 'Max',
        birtDate: '1994-11-14'
    }
];

const tests = [
    {
        id: 1,
        studentId: 1,
        title: 'Fractals',
        score: 0.5
    },
    {
        id: 2,
        studentId: 1,
        title: 'Functional Programming',
        score: 0.75
    },
    {
        id: 3,
        studentId: 2,
        title: 'Functional Programming',
        score: 0.96
    }
];

const StudentTests = tests.map(test => {
    const student = students.find(student => student.id === test.studentId);
    return {
        studentName: student.name,
        testTitle: test.title,
        testScore: test.score
    };
});
console.table(StudentTests);
console.table(StudentTests.filter(studentTest => studentTest.testScore >= 0.7));

const StudentTests2 = tests
    .filter(test => test.score >= 0.7)
    .map(test => {
        const student = students.find(student => student.id === test.studentId);
        return {
            studentName: student.name,
            testTitle: test.title,
            testScore: test.score
        };
});
console.table(StudentTests2);

const StudentTestCount = students.map(student => {
    const filteredTests = tests.filter(test => test.studentId === student.id);
    const testCount = filteredTests.length;
    return {
        StudentName: student.name,
        testCount
    };
});
console.table(StudentTestCount.filter(studentTestCount => studentTestCount.testCount >= 2));

const StudentTestCount2 = students.map(student => {
    const filteredTests = tests.filter(test => test.studentId === student.id && test.score >= 0.7);
    const testCount = filteredTests.length;
    return {
        StudentName: student.name,
        testCount
    };
});
console.table(StudentTestCount2.filter(studentTestCount => studentTestCount.testCount >= 2));
