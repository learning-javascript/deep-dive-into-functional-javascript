// Strict mode in self-executing function
(function () {
    'use strict';
    var lunch = 'Pasta Carbonara';
})();

// Strict mode in block
{
    'use strict';
    const lunch = 'Past Carbonara';
    // lunch = 'Pizza Funghi'; // cause an error
}
