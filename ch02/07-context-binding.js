const rectangleArea = function() {
    return this.width * this.height;
};
console.log(rectangleArea()); // undefined * undefined = NaN

const rectangle = {
    width: 5,
    height: 4
};
// binding rectangle as this for rectangleArea function
console.log(rectangleArea.bind(rectangle)()); // bind() returns a function

const add = (a, b) => {
    console.log(a, b);
    return a + b;
};

const addToFive = add.bind(null, 5);
console.log(addToFive(2));
