// destructuring
const [head, ...tail] = [1, 2, 3];
console.log(head);
console.log(tail);

let right = {
    first: 'Edu',
    last: 'Finn',
    languages: ['JavaScript', 'Java', 'Groovy']
};
const {first, languages: [, ...tailLang]} = right;
console.log(first, tailLang);

const arr = [1, 2, 3];
const getTail = ([, ...tail]) => tail;
console.log(getTail(arr));

// spreading
console.log([...tail, ...tail]);
console.log([...tail, ...tail, 4]);

let logArgs = (a, b, c) => console.log(a, b, c);
logArgs(...tail, ...tail);

let logHeadTail = (head, ...tail) => console.log(head, tail);
logHeadTail(...tail, ...tail);
