{
    'use strict';
    const lunch = 'Past Carbonara';     // String
    const calories = 525;               // Number
    const isHost = true;                // boolean
    const nothing = undefined;          // Undefined
    const emptyObject = null;           // Null
    const ON = Symbol()                 // Symbol
    const indexOutOfArray = [][0];      // Undefined

    console.log('typeof []', typeof []);    // object
    console.log('Array.isArray([])', Array.isArray([])); // true

    const arr = [1, 2, 3];

    // arr = []; // reassignment of const is not possible, but...

    arr[1] = 99; // it is possible to change elements of the array
    console.log('arr', arr);
}
