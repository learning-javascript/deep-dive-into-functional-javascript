// function chaining
console.log(
    ['Functional', 'programming', 'is', 'cool']
);

console.log(
    ['Functional', 'programming', 'is', 'cool']
        .map(word => word.length)
);

console.log(
    ['Functional', 'programming', 'is', 'cool']
        .map(word => word.length)
        .filter(num => num % 2 === 0)
);

console.log(
    ['Functional', 'programming', 'is', 'cool']
        .map(word => word.length)
        .filter(num => num % 2 === 0)
        .reduce((a, b) => a + b, 0)
);

const elevator = {
    floor: 5,
    up() {
        this.floor += 1;
        return this;
    },
    down() {
        this.floor -= 1;
        return this;
    }
};
const newFloor = elevator
                    .up()
                    .up()
                    .down()
                    .up()
                    .floor;
console.log(newFloor);

const elevator1 = {
    floor: 5
};
const up = elevator => {
    return {
        floor: elevator.floor + 1
    };
};
const down = elevator => {
    return {
        floor: elevator.floor - 1
    };
};
const newFloor1 = up(down(up(up(elevator1)))).floor;
console.log(newFloor1);

// function composition
const compose = (...fns) => fns.reduce((f, g) => (...args) => f(g(...args))); 
const move = compose(up, up, down, up);
const newElevator = move(elevator1);
console.log(newElevator.floor);

const sequence2 = (f1, f2) => (...args) => f2(f1(...args));
const f1 = (a, b) => a + b;
const f2 = a => `Result: ${a}`;

console.log(f2(f1(2, 5)));
console.log(sequence2(f1, f2)(2, 5));

const pipe = (...fns) => fns.reduceRight((f,g) => (...args) => f(g(...args)))
console.log(pipe(f1, f2)(2, 5));
