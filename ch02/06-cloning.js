// shallow cloning
const cloneArray = arr => [...arr]; // spread operator copies only top level elements

const originalArray = ['hi', {on: true}];
const newArray = cloneArray(originalArray);

newArray[0] = 'hello';
console.log('original:', originalArray);
console.log('new:', newArray);

newArray[1].on = false; // this will change value of 'on' in both arrays
console.log('original:', originalArray);
console.log('new:', newArray);

// cloning objects
let target = { className: 'js-container' };
let source = { tagName: 'div' };

let combinedObject = Object.assign({}, target, source);
console.log('target:', target);
console.log('source:', source);
console.log('combinedObject:', combinedObject);

// shallow cloning of objects
const cloneObject = o => Object.assign({}, o);

const domNode = {
    className: 'js-container',
    tagName: 'div',
    childNodes: [{ text: 'Hi' }]
};
const clonedNode = cloneObject(domNode);
console.log('domNode:', domNode);
console.log('clonedNode:', clonedNode);

domNode.childNodes[0] = {}; // this will change object in both nodes
console.log('domNode:', domNode);
console.log('clonedNode:', clonedNode);

// deep cloning
const deepClone = o => JSON.parse(JSON.stringify(o)); // this does not clone object's methods

const domNode1 = {
    className: 'js-container',
    tagName: 'div',
    childNodes: [{ text: 'Hi' }]
};
const clonedNode1 = deepClone(domNode1);
console.log('domNode1:', domNode1);
console.log('clonedNode1:', clonedNode1);

domNode1.childNodes[0] = {};
console.log('domNode1:', domNode1);
console.log('clonedNode1:', clonedNode1);
