function mapScoresToGrades(tests, average) {
    // currying
    let getGradeTest = average => ((testScore) => testScore >= average ? 'A' : 'F');
    let gradeTest = getGradeTest(average);
    return tests.map(gradeTest);
}

function mapScoresToGradesFunc(tests, average) {
    // higher order function
    let gradeTest = (testScore) => testScore >= average ? 'A' : 'F';
    return tests.map(gradeTest);
}

function getAverage(tests) {
    return tests.reduce((acc, elm) => acc + elm) / tests.length;
}

function mapScoresToGradesRecursive(tests, average) {
    // recursion
    if (tests.length === 0) return [];
    const head = tests[0];
    const tail = tests.splice(1);
    return [head >= average ? 'A' : 'F'].concat(
        mapScoresToGrades(tail, average)
    );
}

function gradeTests(tests) {
    const average = getAverage(tests);
    return mapScoresToGrades(tests, average);
}
