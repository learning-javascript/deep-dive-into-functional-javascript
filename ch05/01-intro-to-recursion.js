// single recursion example
const factorial = num =>
    num <= 1 ? 1 : num * (factorial(num - 1));

console.log('1)', factorial(100));
console.log('2)', factorial(1000)); // result is to big
// next call causes RangeError: Maximum call stack size exceeded
// console.log(factorial(100000));

// multiple recursion example
const tree = {
    value: 5,
    left: null,
    right: {
        value: 8,
        left: {
            value: 6,
            left: null,
            right: null
        },
        right: {
            value: 11,
            left: null,
            right: null
        }
    }
};

const sumElements = tree =>
    tree === null ? 0 : tree.value + sumElements(tree.left) + sumElements(tree.right);

console.log('3)', sumElements(tree));

const iterativeSumElements = function(tree) {
    let sumOfNodes = 0;
    let nodes = [tree];

    while(nodes.length > 0) {
        const node = nodes.pop();
        if (node !== null) {
            sumOfNodes += node.value;
            nodes.push(node.left);
            nodes.push(node.right);
        }
    }

    return sumOfNodes;
};

console.log('4)', iterativeSumElements(tree));
