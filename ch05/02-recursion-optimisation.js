// direct single tail recursion
const tree = {
    value: 5,
    left: null,
    right: {
        value: 8,
        left: {
            value: 6,
            left: null,
            right: null
        },
        right: {
            value: 11,
            left: null,
            right: null
        }
    }
};

const tcoSumElements = tree =>
    tcoSumNodeList([tree], 0);

const tcoSumNodeList = ([head, ...tail], sum) =>
    typeof head === 'undefined' ? sum :
        head === null ? tcoSumNodeList(tail, sum) : tcoSumNodeList([head.left, head.right, ...tail], sum + head.value);

console.log('1)', tcoSumElements(tree));

// momoization
const memo = f => {
    let memoMap = new Map();
    // window.m = memoMap; // can be tested in browser
    return fArg => memoMap.has(fArg) ? memoMap.get(fArg) : memoMap.set(fArg, f(fArg)).get(fArg);
};

let factorial = num =>
    num <= 1 ? 1 : num * factorial(num - 1);

factorial = memo(factorial);

console.log('2)', factorial(10));
